from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
# from django.contrib.auth import authenticate, login, logout
# from accounts.form import LoginForm, SignUpForm
from accounts.form import SignUpForm, LoginForm


# Create your views here.


# Refer to D4 > Scrumptious > Logging In for more details

def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
#                                  ^
#              Be careful if you change my break the paths


    else:
        form = LoginForm()
    context = {
        "login": form,
    }
    return render(request, "accounts/login.html", context)


# Refer to D4 > Scrumptious > Logging out for more details

def user_logout(request):
    logout(request)
    return redirect("login")



# Refer to D4 Scrumptious > Letting People Signup for more details

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            if password == password_confirmation:
                # Creat a new user with those values

                user = User.objects.create_user(
                    username,
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                )

                # and save it to a variable

                # Login the user the use you just
                login(request, user)
                # created

                return redirect("login")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context ={
        "signup": form,
    }

    return render(request, "accounts/ signup.html", context)

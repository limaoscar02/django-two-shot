from django.contrib.auth.models import User
from django.forms import Form
from django import forms



# Refer to D4 Scrumptious > Loggin In for more details
class LoginForm(forms.Form):
        username = forms.CharField(max_length=150)
        password = forms.CharField(
                max_length=150,
                widget=forms.PasswordInput,
                )


# Refer to D4 Scrumptious > Letting People Signup for more details
class SignUpForm(forms.Form):
        username = forms.CharField(max_length=150)
        first_name = forms.CharField(max_length=150)
        last_name = forms.CharField(max_length=150)
        password = forms.CharField(
                max_length=150,
                widget=forms.PasswordInput,
        )
        password_confirmation = forms.CharField(
                max_length=150,
                widget=forms.PasswordInput,
        )

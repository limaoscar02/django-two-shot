from django.db import models

# Create your models here.

# After each models change in any app directory you must migrate
# python mange.py makemigration
# python mange.py migrate


class AccountsList(models.Model):
    name = models.CharField(max_length=100)
    create_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name


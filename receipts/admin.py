from django.contrib import admin
from receipts.models import ReceiptsList
from receipts.models import ExpenseCategory
from receipts.models import Receipt
from receipts.models import Account

# Register your models here.
# Don't for get to migrate
# eg python manage.py makemigrations
# eg python manage.py migrate


@admin.register(ReceiptsList)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",

    )

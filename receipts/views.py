from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
# Create your views here.

def receipt_list(request):
    receipts = Receipt.objects.all()
    context = {
        "receipts_list": receipts,
    }
    return render(request, "receipts/receipts_list.html", context)

# Review D4: Scrumptious > My Recipes, for more details

@login_required
def my_receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "my_receipts_list": receipts
    }
    return render(request, "receipts/receipts_list.html", context)

# def receipt_list(request):
#     receipts = Receipt.objects.all()
#                   ^
#                model.py class variable

#     context = {
#         "receipts_list": receipts,
#                  ^
#       html reference this str
# }
#     return render(request, "receipts/receipts_list.html", context)
#                                       ^
#                      This tell html the path of folders to file

from django.urls import path
from receipts.views import my_receipts_list

urlpatterns = [
    path("", my_receipts_list, name="home"),
]
#                                      ^
#                               Be careful if you change my break the paths

#     path("",receipt_list, name="home")
#          ^         ^
# This adds additionl path layer, this looks into views.py

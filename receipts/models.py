from django.db import models
from django.conf import settings

# Create your models here.

# After each models change in any app directory you must migrate
# python mange.py makemigration
# python mange.py migrate

class ReceiptsList(models.Model):
    name = models.CharField(max_length=100)
    create_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE                    #<---this allow for casade deletion relations, meaning if you delete it remove across other areas##
    )

class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField( decimal_places=3, max_digits=10)
    tax = models.DecimalField( decimal_places=3, max_digits=10)
    date =models.DateTimeField(auto_now_add=True)
    purchaser =models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        "ExpenseCategory", related_name="receipts",
        on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        "Account", related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )

class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE
    )
